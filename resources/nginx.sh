#!/bin/sh

cd "$(dirname "$0")"

mkdir /var/log/nginx
chown -R nginx:nginx /var/log/nginx


#self signed certificate
cp keys/nginx.key /etc/ssl/private/nginx.key
cp keys/nginx.crt /etc/ssl/certs/nginx.crt
#copy all config files
cp nginx/* /etc/nginx/conf.d/

#flush systemd cache
systemctl daemon-reload

#restart nginx
service nginx restart